<?php
$type = get_sub_field('com_logos_type');
$logos = get_sub_field('com_logos_icons');

if ($type === 'clients'){
    $logos = [];
    $objects = get_sub_field('com_logos_clients');
    foreach ($objects as $obj) $logos[]['ID'] = get_field('customers_basic_logo', $obj);
}
if ($type === 'services'){
    $logos = [];
    $objects = get_sub_field('com_logos_services');
    foreach ($objects as $obj) $logos[]['ID'] = get_field('services_basic_icon', $obj);
}

?>
<?php if(get_sub_field('com_logos_slider') && $logos): ?>
    <div class="mode-slider">
        <div class="market-man">
            <div class="market-man-text">
                <div class="container">
                    <div class="col-md-12 text-center">
                        <div class="synchronization">
                            <img src="<?php module_img('components/synchronization.png'); ?>" alt="synchronization">
                        </div>
                        <h1 class="title"><?php the_sub_field('com_logos_title'); ?></h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="market-man-all">
            <div class="col-md-12">
                <div class="carousel slide media-carousel" id="media">
                    <div class="carousel-inner">
                        <div class="item active">
                            <div class="row logos-carousel">
                                <?php
                                $count = count($logos);
                                $counter = 0;
                                ?>
                                <?php if ($logos): ?>
                                    <div class="slide">
                                    <?php foreach ($logos as $logo): $counter++; ?>
                                        <?php echo wp_get_attachment_image($logo['ID'], 'full', false, array('class' => 'icon')); ?>
                                        <?php if($counter % 8 === 0 && ($count - $counter) !== 0 ): ?>
                                            </div><div class="slide">
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
<?php endif; ?>
<?php if(!get_sub_field('com_logos_slider') && $logos): ?>
    <div class="mode-logos">
        <div class="clear"></div>
        <h1 class="p-header"><?php the_sub_field('com_logos_title'); ?></h1>
        <div class="clear"></div>
        <div class="container">
            <div class="row text-center">
                <?php foreach ($logos as $logo): $counter++; ?>
                    <div class="m"><?php echo wp_get_attachment_image($logo['ID'], 'full'); ?></div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
<?php endif; ?>

#### IMPORTANT! Set on local `php_value max_input_vars 100000`

### How to run
- Pull "master" branch;
- Set public directory as root directory in php config;
- Run `composer update` command;
- Run `npm install` command;
- Create empty database;
- Configure wp-config.php, set database name from previous step;
- To import database use cli-tool:
	- Paste sql file in project directory (not public dir);
	- Run `sh cli/wp db import "filename.sql"` command;
	- Run `sh cli/wp search-replace "old.site.com" "new.site.com"` command to replace hostnames in database;

### How to compile SCSS
- Run `grunt` command to call sass watcher.
- All SCSS located in `public/wp-content/themes/common/modules/**/assets/scss/*.scss`

### Architecture
This site based on modules architecture. All modules located in `public/wp-content/themes/common/modules` dir.
All modules should be located in `public/wp-content/themes/common/modules/modulename` dir and will be include automatically. 
#### Module dir structure
    /assets/img/ - Images
    /assets/js/ - JavaScript scripts
    /assets/css/ - Compiled css (don't change and delete dir or files in this dir)
    /assets/scss/ - SASS styles
    /assets/scss/front.scss - Front file which includes all SCSS submodules.
    /front/front.php - This file requires by functions.php core file. Used for hooks, etc.
    /templates/ - Templates dir. Can called by module_template function. Ex: module_template('modulename/templatename.php');

### Components
Based on Advanced Custom Fields plugin. Located in `public/wp-content/themes/common/modules/components/` dir. It is possible to create new components from admin panel. Select `Custom Fields` in admin left menu, then select `components`, after click on `components` fields group. You can clone existing component and rename or click on add new button. It is located in left side if you hovered on existing component. Component template name must begin from `_com` prefix.

**IMPORTANT!!!** Dont miss experiment variations field. It is part of experiment pages system.

### Plugins
Plugins will install automatically after run composer command. To add new plugin, insert line like `"wpackagist-plugin/wordpress-seo": "3.6",` to composer.json file. You can find all free wp plugins for composer on this site: http://wpackagist.org.

(function($) {
    var menuSelector = $('.top-nav-menu.index');
    if($(menuSelector).length > 0){
        $(window).scroll(function () {
            if ($(this).scrollTop() > 50) $(menuSelector).addClass('black');
            if ($(this).scrollTop() > 50) $(menuSelector).removeClass('mob-black');
            if ($(this).scrollTop() <= 50) $(menuSelector).addClass('mob-black');
            if ($(this).scrollTop() <= 50) $(menuSelector).removeClass('black');
        });
    }

    $('.nav.navbar-nav li').mouseover(function() {
        $(this).addClass('open');
    }).mouseleave(function() {
        $(this).removeClass('open');
    });

    $('.nav.navbar-nav li a').click(function() {
        window.location = $(this).attr('href');
    });

    function showMenuAsTree() {
        if ($('.navbar-toggle:visible').length > 0){
            $('.nav.navbar-nav li').addClass('open');
        }else{
            $('.nav.navbar-nav li').removeClass('open');
        }
    }

    $(window).on('load resize', function(){
        showMenuAsTree();
    });

    $('#bs-example-navbar-collapse-1').on('show.bs.collapse', function () {
        setTimeout(showMenuAsTree, 1);
    })

})(jQuery);

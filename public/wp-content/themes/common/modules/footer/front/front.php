<?php

add_action('wp_print_styles', function () {
    wp_enqueue_style('footer', get_module_css('footer/front.css'), array(), nix_get_rev());
});

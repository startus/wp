<?php get_header(); ?>
<?php $page_id = get_queried_object_id(); ?>

<div class="blog-template">
    <div class="clear"></div>

    <?php
        $tagData = get_the_ID();
        $args = array(
            'post_type' => 'post',
            'meta_key' => 'blog_basic_tags',
            'meta_value' => '"'.$tagData.'"',
            'meta_compare' => 'LIKE',
            'nopaging' => true
        );
        $query = new WP_Query( $args );
    ?>

    <div class="container">
        <div class="blog-draw">
            <div class="clear"></div>
            <?php $counter = 0; ?>
            <?php while($query->have_posts()): $query->the_post(); $counter++; ?>
                <div class="blog-draw-data">
                    <div class="col-md-6">
                        <div class="blog-page">
                            <a href="<?php the_permalink(); ?>">
                                <?php echo wp_get_attachment_image(get_field('blog_basic_image', get_the_ID()), 'wp-blog', false, array('class' => 'blog-page-img')); ?>
                                <h2 class="blog-page-header"><?php echo get_the_title(); ?></h2>
                            </a>
                            <h3 class="blog-page-d"><?php the_field('blog_basic_short_description', get_the_ID()); ?></h3>
                            <div class="read">
                                <a class="read-more" href="<?php the_permalink(); ?>">Read more</a>
                                <p class="share">Share</p>
                                <p class="share">Share <?php echo do_shortcode('[addtoany url="'.get_the_permalink().'" title="'.get_the_title().'"]'); ?></p>
                            </div>
                        </div>
                    </div>
                    <?php if($counter % 2 === 0): ?><div class="clearfix"></div><?php endif; ?>
                </div>
            <?php endwhile; ?>

            <?php wp_reset_postdata(); ?>
        </div>
        <div class="clearfix"></div>
        <div class="blog-f">

            <?php $query = new WP_Query( array('post_type' => array('tags'), 'posts_per_page' => -1, 'post_status' => 'publish') ); ?>
            <?php while($query->have_posts()): $query->the_post(); ?>
                <a class="teg" href="<?php the_permalink(); ?>"><?php echo get_the_title(); ?></a>
            <?php endwhile; ?>
            <?php wp_reset_postdata(); ?>
        </div>

        <div class="clear"></div>
    </div>

    <div class="start-saving">
        <div class="container">

            <div class="clear"></div>

            <p class="start-saving-top">Start saving today! Try our <strong class="green">Free</strong> Trial</p>

            <div class="clear"></div>

            <div class="row">

                <?php echo do_shortcode('[contact-form-7 id="223" title="Contact Us"]'); ?>

            </div>

            <div class="clear"></div>

            <p class="start-saving-bottom">By clicking «Get Started» I agree to MarketMan’s Terms &amp; Conditions</p>

            <div class="clear"></div>

        </div>
    </div>
</div>

<?php get_footer() ?>

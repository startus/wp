#!/usr/bin/env bash

if [ -z $1 ]; then
    echo "Usage: $0 <dev|production>"
    exit 1
fi

STACK=$1
BASEDIR=$(dirname $0)
DEPLOY_FOLDER="/var/www/html/$STACK"
UPLOADS_FOLDER="/var/www/html/fileshare"

if [ $STACK = 'production' ]; then
    OPTION_BLOG_PUBLIC=1
else
    OPTION_BLOG_PUBLIC=0
fi

composer install
grunt prod

rsync -avz --delete --no-perms -O -e "ssh" \
    --exclude='*.idea' \
    --exclude='*.git' \
    --exclude='*.gitignore' \
    --exclude='*.DS_Store' \
    --exclude='/README.md' \
    --exclude='/.htaccess' \
    --exclude='/composer*' \
    --exclude='/bin/deploy' \
    --exclude='/bin/local' \
    --exclude='/composer-artifacts' \
    --exclude='/.sass-cache' \
    --exclude='/node_modules' \
    --exclude='/public/wp/wp-content/*' \
    --exclude='/public/wp-config.php' \
    --exclude='/public/wp-content/uploads' \
    --exclude='/public/wp-content/cache/*/*' \
    --exclude='/wp-content/themes/common/modules/contact-us/server/php/files/*' \
    --exclude='/public/wp-content/plugins/bwp-minify/cache/*' \
    --exclude='/public/wp-content/plugins/w3tc-wp-loader.php' \
    . nixftp@n-ix.com:$DEPLOY_FOLDER

ssh nixftp@n-ix.com "ln -fs $UPLOADS_FOLDER $DEPLOY_FOLDER/public/wp-content/uploads"
ssh nixftp@n-ix.com "rm -f $DEPLOY_FOLDER/public/wp-config.php"
ssh nixftp@n-ix.com "ln -fs $DEPLOY_FOLDER/public/wp-config-$STACK.php $DEPLOY_FOLDER/public/wp-config.php"

#ssh nixftp@n-ix.com "rm -f $DEPLOY_FOLDER/public/wp-content/advanced-cache.php"
#ssh nixftp@n-ix.com "ln -fs $DEPLOY_FOLDER/public/wp-content/plugins/w3-total-cache/wp-content/advanced-cache.php $DEPLOY_FOLDER/public/wp-content/advanced-cache.php"

#ssh nixftp@n-ix.com "rm -f $DEPLOY_FOLDER/public/wp-content/object-cache.php"
#ssh nixftp@n-ix.com "ln -fs $DEPLOY_FOLDER/public/wp-content/plugins/w3-total-cache/wp-content/object-cache.php $DEPLOY_FOLDER/public/wp-content/object-cache.php"

ssh nixftp@n-ix.com "chown -R nixftp:apache $DEPLOY_FOLDER/"
ssh nixftp@n-ix.com "find $DEPLOY_FOLDER/ -type d -exec chmod 770 {} \;"
ssh nixftp@n-ix.com "find $DEPLOY_FOLDER/ -type f -exec chmod 660 {} \;"
ssh nixftp@n-ix.com "chmod 440 $DEPLOY_FOLDER/public/wp-config*.php"
ssh nixftp@n-ix.com "chmod -R 770 $DEPLOY_FOLDER/public/wp-content/uploads"
ssh nixftp@n-ix.com "chmod -R 770 $DEPLOY_FOLDER/public/wp-content/cache"
ssh nixftp@n-ix.com "chmod -R 777 $DEPLOY_FOLDER/public/wp-content/ewww"
ssh nixftp@n-ix.com "mkdir $DEPLOY_FOLDER/public/wp-content/wp-rocket-config"
ssh nixftp@n-ix.com "chmod -R 777 $DEPLOY_FOLDER/public/wp-content/wp-rocket-config"

ssh nixftp@n-ix.com "cd $DEPLOY_FOLDER && sh cli/wp option update blog_public $OPTION_BLOG_PUBLIC"
ssh nixftp@n-ix.com "cd $DEPLOY_FOLDER && sh cli/wp cache flush"

<div id="login" class="modalLogin">
    <div>
        <a href="#close" class="close"><img src="<?php module_img('header/close.png'); ?>" alt="close"></a>
        <p class="login-header">Login to MarketMan</p>

        <div class="form-group">
            <label>Email</label>
            <input type="text" size="40" placeholder="Email">
        </div>

        <div class="form-group">
            <label>Password</label>
            <input type="text" size="40" placeholder="Password">
        </div>

        <a class="forgot" href="#forgot">Forgot your password?</a>
        <a class="grn-button" href="/">Login</a>

        <div class="new-account">
            <p class="an-account">Don’t have an account?</p>
            <a class="start-free" href="#start-saving">Start free trial</a>
        </div>
    </div>
</div>

<div id="forgot" class="modalLogin">
    <div>
        <a href="#close" class="close"><img src="<?php module_img('header/close.png'); ?>" alt="close"></a>
        <p class="login-header">Forgot your password?</p>
        <p class="login-well">We’ll send you an email with reset link</p>

        <div class="form-group">
            <label>Email</label>
            <input type="text" size="40" placeholder="Email">
        </div>
        <a class="grn-button" href="/">Login</a>
    </div>
</div>

<div id="start-saving" class="modalStartSaving">
    <div>
        <a href="#close" class="close"><img src="<?php module_img('header/close.png'); ?>" alt="close"></a>
        <p class="login-header">Start saving today</p>
        <div class="row">
            <div class="col-md-6">

                <div class="form-group">
                    <label>Your Name</label>
                    <input type="text" size="40" placeholder="Your Name">
                </div>

                <div class="form-group">
                    <label>Business Name</label>
                    <input type="text" size="40" placeholder="Business Name">
                </div>

                <div class="form-group">
                    <label>Email</label>
                    <input type="text" size="40" placeholder="Email">
                </div>

                <div class="form-group">
                    <label>Phone</label>
                    <input type="text" size="40" placeholder="Phone Number">
                </div>

                <a class="grn-button" href="/">Get Started</a>

                <div class="new-account">
                    <p class="an-account">By clicking «Get Started» I agree </br>to <a href="">MarketMan’s Terms of Service</a></br>Already have an account?
                        <a class="login-here" href="#login">Login Here<a></p>
                </div>
            </div>

            <div class="col-md-6">
                <figure class="dir-rtl-hidden"><img src="<?php module_img('header/startsaving.png'); ?>" alt="start saving"></figure>
            </div>
        </div>
    </div>
</div>


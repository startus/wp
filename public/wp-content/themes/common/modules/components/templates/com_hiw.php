<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="pg-h"><?php the_sub_field('com_hiw_title'); ?></div>
            <div class="clear"></div>
            <div class="col-md-3">
                <div class="pg-bg-bl">
                    <p class="pg-bg-bl-h"><?php the_sub_field('com_hiw_title_one'); ?></p>
                    <p class="pg-bg-bl-text"><?php the_sub_field('com_hiw_description_one'); ?></p>
                </div>
            </div>
            <div class="col-md-3">
                <div class="pg-bg-bl">
                    <p class="pg-bg-bl-h"><?php the_sub_field('com_hiw_title_two'); ?></p>
                    <p class="pg-bg-bl-text"><?php the_sub_field('com_hiw_description_two'); ?></p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="pg-bg-bl">
                    <p class="pg-bg-bl-h"><?php the_sub_field('com_hiw_title_three'); ?></p>
                    <p class="pg-bg-bl-text"><?php the_sub_field('com_hiw_description_three'); ?></p>
                </div>
            </div>
            <div class="pg-center">
                <img src="<?php module_img('components/man.png'); ?>" alt="man">

                <div class="clear"></div>

                <?php if( have_rows('com_hiw_features') ): ?>
                    <?php while( have_rows('com_hiw_features') ): the_row(); ?>
                        <div class="col-md-4">
                            <a href="#" class="grn-button-2"><?php the_sub_field('com_hiw_features_title'); ?></a>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>

                <div class="clear"></div>

                <a href="/" class="button-green-s">Free Trial</a>
            </div>
        </div>
    </div>

    <div class="clear"></div>
</div>
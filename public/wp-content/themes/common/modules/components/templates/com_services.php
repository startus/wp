<?php global $geodata; ?>
<div class="mode-slider">
    <div class="market-man">
        <div class="market-man-text">
            <div class="container">
                <div class="col-md-12 text-center">
                    <div class="synchronization">
                        <img src="<?php module_img('components/synchronization.png'); ?>" alt="synchronization">
                    </div>
                    <h1 class="title"><?php the_sub_field('com_services_title'); ?></h1>
                </div>
            </div>
        </div>
    </div>
    <div class="market-man-all">
        <div class="col-md-12">
            <div class="carousel slide media-carousel" id="media">
                <div class="carousel-inner">
                    <div class="item active">
                        <div class="row logos-carousel">
                            <?php $services = get_posts(array('numberposts' => -1, 'post_type' => 'services')); ?>
                            <?php $filtered = array(); ?>
                            <?php foreach($services as $service): ?>
                                <?php $geogroup = get_field('page_basic_geolocation', $service->ID); ?>
                                <?php if (!$geogroup || !$geodata['country_code'] || in_array($geodata['group'], $geogroup)): ?>
                                    <?php $filtered[] = $service->ID; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>

                            <?php
                            $count = count($filtered);
                            $counter = 0;
                            ?>

                            <div class="slide">
                            <?php foreach ($filtered as $fid): $counter++; ?>
                                <?php echo wp_get_attachment_image(get_field('services_basic_icon', $fid), 'full', false, array('class' => 'icon')); ?>
                                <?php if($counter % 8 === 0 && ($count - $counter) !== 0 ): ?>
                                    </div><div class="slide">
                                <?php endif; ?>
                            <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>

<?php global $geodata; ?>
<div class="container layout-testimonials">
    <div class="clear"></div>
    <h1 class="p-header"><?php the_sub_field('com_testimonials_title'); ?></h1>
    <?php if(get_sub_field('com_testimonials_description')): ?>
        <h2 class="p-description"><?php the_sub_field('com_testimonials_description'); ?></h2>
    <?php endif; ?>
    <div class="clear"></div>
    <div class="block-center">
        <?php
        $testimonials = get_posts(array('numberposts' => -1, 'post_type' => 'testimonials'));
        $counter = 0;
        ?>

        <?php foreach($testimonials as $testimonial): ?>
            <?php $geogroup = get_field('page_basic_geolocation', $testimonial->ID); ?>
            <?php if (!$geogroup || !$geodata['country_code'] || in_array($geodata['group'], $geogroup)): ?>
                <?php $counter++; ?>
                <div class="col-md-4">
                    <div class="block-margin left-content">
                        <a href="<?php the_permalink($testimonial->ID); ?>">
                            <?php if(get_field('testimonials_basic_image', $testimonial->ID)): ?>
                                <?php echo wp_get_attachment_image(get_field('testimonials_basic_image', $testimonial->ID), 'wp-blog'); ?>
                            <?php endif; ?>

                            <div class="title"><?php echo get_the_title($testimonial->ID); ?></div>
                        </a>
                        <div class="description"><?php the_field('testimonials_basic_short_testimonial', $testimonial->ID); ?></div>
                    </div>
                </div>
                <?php if($counter % 3 === 0): ?><div class="clearfix"></div><?php endif; ?>
            <?php endif; ?>
        <?php endforeach; ?>
    </div>
</div>
<div class="clear"></div>

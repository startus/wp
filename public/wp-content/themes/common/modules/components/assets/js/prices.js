(function($) {
    $('.toggles-block input[type=checkbox]').click(function() {
        if($(this).is(':checked')){
            $('.prices.price-month').addClass('hidden');
            $('.prices.price-year').removeClass('hidden');
        }else{
            $('.prices.price-month').removeClass('hidden');
            $('.prices.price-year').addClass('hidden');
        }
    });
})(jQuery);

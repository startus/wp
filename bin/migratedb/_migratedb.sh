#!/usr/bin/env bash

BASE_DIR=`cd $(dirname $0); pwd`
DUMPFILE="/tmp/nixmydump.sql"
CLIDIR="cli/wp"

FROM_STACK=$1
TO_STACK=$2
DEV_DIR="$BASE_DIR/../../../dev/"
PROD_DIR="$BASE_DIR/../../../production/"
LOCAL_DIR="$BASE_DIR/../../"
DEV_URL="http://dev.n-ix.com"
PROD_URL="http://n-ix.com"
LOCAL_URL="http://n-ix.lh"


if [ $FROM_STACK = "production" ]; then
    FROM_DIR=$PROD_DIR
    FROM_URL=$PROD_URL
elif [ $FROM_STACK = "dev" ]; then
    FROM_DIR=$DEV_DIR
    FROM_URL=$DEV_URL
else
    FROM_DIR=$LOCAL_DIR
    FROM_URL=$LOCAL_URL
fi

if [ $TO_STACK = 'production' ]; then
    TO_DIR=$PROD_DIR
    TO_URL=$PROD_URL
elif [ $TO_STACK = "dev" ]; then
    TO_DIR=$DEV_DIR
    TO_URL=$DEV_URL
else
    TO_DIR=$LOCAL_DIR
    TO_URL=$LOCAL_URL
fi


cd $FROM_DIR
sh $CLIDIR db export $DUMPFILE

cd $TO_DIR
sh $CLIDIR db reset --yes
sh $CLIDIR db import $DUMPFILE
sh $CLIDIR search-replace $FROM_URL $TO_URL

rm $DUMPFILE

if [ $TO_STACK = 'production' ]; then
    OPTION_BLOG_PUBLIC="1"
else
    OPTION_BLOG_PUBLIC="0"
fi
sh $CLIDIR option update blog_public $OPTION_BLOG_PUBLIC

sh $CLIDIR cache flush

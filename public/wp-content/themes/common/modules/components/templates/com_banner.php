<?php
$elements = get_sub_field('com_banner_elements');
$type = get_sub_field('com_banner_type') ?? 'default';
$animated = get_sub_field('com_banner_animated');
?>
<div class="banner-index layout-<?php echo $type; ?>" style="background-image: url('<?php the_sub_field('com_banner_background'); ?>');">
    <div class="border-part">
        <div class="container">
            <div class="col-md-12">
                <div class="banner-text">
                    <?php if(get_sub_field('com_banner_title')): ?>
                        <h1 class="p-banner-text<?php if(!empty($animated)): ?> animatedbn<?php endif; ?>"><?php the_sub_field('com_banner_title'); ?></h1>
                    <?php endif; ?>

                    <?php if(get_sub_field('com_banner_description')): ?>
                        <h2 class="p-description-banner-text"><?php the_sub_field('com_banner_description'); ?></h2>
                    <?php endif; ?>

                    <?php if($elements && in_array('button', $elements)): ?>
                        <a href="<?php the_sub_field('com_banner_button_link'); ?>" class="button-radius">
                            <?php the_sub_field('com_banner_button_title'); ?>
                        </a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>

    <?php if($elements && in_array('promovideo', $elements)): ?>
        <div class="see" data-toggle="modal" data-target="#videoModal">
            <img src="<?php module_img('components/play.png'); ?>" alt="play">
            <p>See MarketMan in action</p>
        </div>

        <div class="modal fade" id="videoModal" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <iframe width="100%" height="500" src="<?php the_sub_field('com_banner_video_link'); ?>" frameborder="0" allowfullscreen></iframe>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <div class="clearfix"></div>
</div>

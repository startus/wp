<div class="clear"></div>
<div class="clear"></div>
<div class="container">
    <h1 class="p-header"><?php the_sub_field('com_pricing_title'); ?></h1>
    <h2 class="p-header-mini"><?php the_sub_field('com_pricing_description'); ?></h2>

    <div class="toggles-block-center">
        <ul class="toggles-block-w">
            <li><p class="monthly">Monthly</p></li>
            <li>
                <div class="toggles-block">
                    <input type="checkbox">
                </div>
            </li>

            <li><p class="Yearly">Yearly</p></li>
            <li><img class="save" src="<?php module_img('components/save.png'); ?>" alt="save"></li>
        </ul>
    </div>

    <div class="col-md-4">
        <?php the_sub_field('com_pricing_new_feature'); ?>
    </div>

    <div class="col-md-4">
        <?php the_sub_field('com_pricing_most_popular'); ?>
    </div>


    <div class="col-md-4">
        <?php the_sub_field('com_pricing_special_offer'); ?>

        <hr>

        <?php the_sub_field('com_pricing_enterprise'); ?>
    </div>

    <h1 class="p-header"><?php the_sub_field('com_pricing_qtitle'); ?></h1>

    <div class="clear"></div>

    <?php while( have_rows('com_pricing_questions') ): the_row(); ?>
        <div class="col-md-6">
            <div class="pricing-block">
                <h2 class="pricing-h"><?php the_sub_field('com_pricing_questions_title'); ?></h2>
                <h3 class="pricing-d"><?php the_sub_field('com_pricing_questions_description'); ?></h3>
            </div>
        </div>
    <?php endwhile; ?>
    <div class="clearfix"></div>

    <p class="p-header"><?php the_sub_field('com_pricing_hqtitle'); ?></p>

    <div class="button-green-s-center">
        <a href="<?php the_sub_field('com_pricing_ask_link'); ?>" class="button-green-s"><?php the_sub_field('com_pricing_ask_title'); ?></a>
    </div>

    <div class="clear"></div>
</div>

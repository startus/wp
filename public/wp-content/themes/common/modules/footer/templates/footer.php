<footer>
    <div class="container">

        <div class="top">

            <div class="col-md-6">
                <div class="flogo"><a href="/"><img src="<?php module_img('footer/logo.png'); ?>" alt="footer logo"></a></div>
            </div>

            <div class="col-md-6">
                <?php wp_nav_menu(array(
                    'theme_location' => 'footer',
                    'menu_class' => 'nav navbar-nav'
                )); ?>
            </div>
        </div>

        <div class="footer-block1">
            <?php
            $locations = get_nav_menu_locations();
            $menu_id = $locations['footer_phones'] ;
            $links = wp_get_nav_menu_items($menu_id);
            ?>
            <?php foreach ($links as $link): ?>
                <div class="col-md-3">
                    <div class="block-margin">
                        <p class="phone<?php if($link->classes): ?> <?php echo implode(' ', $link->classes); ?><?php endif; ?>"><?php echo $link->post_content; ?> <strong class="green"><?php echo $link->post_title; ?></strong></p>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>


        <div class="footer-block2">

            <div class="col-md-9">
                <p class="footer-text">Marketman is a collaboration software between foodservice operators and their suppliers. The system is in use by full service restaurants,
                    quick service restaurants, coffee shops, bars, food trucks, bakeries, and manages the procurement and supply from product catalog and prices, through the delivery and accounting. Marketman was built out of desire to help businesses streamline processes and save money. MarketMan helps multi-unit operators and independent restaurants to improve their bottom line.</p>

                <div class="col-md-3">
                    <div class="row">
                        <ul class="soc">
                            <li><a href="#"><img src="<?php module_img('footer/fb.png'); ?>" alt="facebook"></a></li>
                            <li><a href="#"><img src="<?php module_img('footer/ln.png'); ?>" alt="linkedin"></a></li>
                            <li><a href="#"><img src="<?php module_img('footer/tw.png'); ?>" alt="twitter"></a></li>
                            <li><a href="#"><img src="<?php module_img('footer/google.png'); ?>" alt="google"></a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-md-9">
                    <p class="footer-powerd">Copyright@2015. All Rights Reserved. Terms &amp; Conditions, Privacy Policy, 中文, Español</p>
                </div>

            </div>

            <div class="col-md-3">
                <div class="apps">
                    <a href="/"><img src="<?php module_img('footer/google-play.png'); ?>" alt="google play"></a>
                    <a href="/"><img src="<?php module_img('footer/app-store.png'); ?>" alt="app store"></a>
                </div>
            </div>

        </div>
    </div>
</footer>

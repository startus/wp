<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="icon" href="<?php echo get_template_directory_uri().'/modules/theme/assets/img/'; ?>favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri().'/modules/theme/assets/img/'; ?>favicon.ico" type="image/x-icon" />
    <title><?php echo get_bloginfo( 'name' ); ?> <?php wp_title(); ?></title>
    <?php wp_head(); ?>
<body <?php body_class(); ?>>
    <div<?php if(get_field('advanced_settings_rtl', 'option')): ?> class="rtl-page"<?php endif; ?>>
    <?php module_template('header/header'); ?>
    <?php module_template('header/menu'); ?>
